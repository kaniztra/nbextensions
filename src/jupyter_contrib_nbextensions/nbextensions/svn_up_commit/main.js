define(['base/js/namespace','base/js/dialog','jquery'],function(IPython, dialog, $, mc){
    var url = window.location.href;
    var pb_data = url.split("/");
    var project_dir = '/home/'+pb_data[4]+'/projects';
    String.prototype.replaceAll = function(search, replace){
        return this.split(search).join(replace);
      }
        var svn_up  = {
            help: 'spv update',
            icon: 'svn_up',
            handler : function (env) {
                var div = $('<p class="svnup_content text-center"><i class="fa fa-spinner fa-spin fa-3x center-block"></i></p><strong class="text-success svn_success"></strong>')
                    dialog.modal({
                    body: div ,
                    title: 'Svn up',
                    notebook:env.notebook,
                    keyboard_manager: env.notebook.keyboard_manager,
                    open : function (event, ui) {
                        var that = $(this);
                                            var cb = {}
                        cb.iopub = {}
                        cb.iopub.output = function(msg) {
                            if (tm){
                                clearTimeout(tm)
                            }
                            var tm = setTimeout(function(){ $('.svnup_content').parent().parent().find('.close').click(); }, 5000);
                            $('.svnup_content i').remove()
                            $('.svnup_content').removeClass('text-center')
                            $('.svnup_content').html($('.svnup_content').html()+msg.content.text.replace(/(?:\r\n|\r|\n)/g, '<br />'))
                            if (msg.content.text.indexOf('Updated to revision')!==-1 || msg.content.text.indexOf('At revision ')!==-1) {
                            $('.svnup_content').removeClass('text-danger')
                            $('.svnup_content').addClass('text-success')
                            $('.svn_success').text('Success');
                            }else {
                                $('.svnup_content').removeClass('text-success')
                                $('.svnup_content').addClass('text-danger')
                            }
                            
                        }
                        var kernel = Jupyter.notebook.kernel;
                        notebook_path = project_dir+"/"+Jupyter.notebook.notebook_path
                        kernel.execute("!cd "+project_dir+" && svn up "+notebook_path, cb);
                        
                                            
                        
                }
                })
                }
        }
        
        
        
        
        var svn_commit  = {
            help: 'Commit notebook to svn',
            icon : 'svn_commit',
            help_index : '',
            handler : function (env) {
                var on_success = undefined;
                var on_error = undefined;
    
                var p = $('<p/>').text("Please, enter your commit message (optional).")
                var input = $('<textarea rows="4" cols="89"></textarea>')
                var div = $('<div/>')
    
                div.append(p)
                   .append(input)
    
                // get the canvas for user feedback
                var container = $('#notebook-container');
    
                function on_ok(){
                    var commit_text = $('.modal textarea').val();
                    var div = $('<p class="svncomm_content text-center"><i class="fa fa-spinner fa-spin fa-3x center-block"></i></p><strong class="text-success svn_success"></strong>')
                                dialog.modal({
                    body: div ,
                    title: 'Svn Commit',
                    notebook:env.notebook,
                    keyboard_manager: env.notebook.keyboard_manager,
                    open : function (event, ui) {
                        var that = $(this);
                                            var cb = {}
                        cb.iopub = {}
                        cb.iopub.output = function(msg) {
                            if (tm){
                                clearTimeout(tm)
                            }
                            var tm = setTimeout(function(){ $('.svncomm_content').parent().parent().find('.close').click(); }, 5000);
                            $('.svncomm_content i').remove()
                            $('.svncomm_content').removeClass('text-center')
                            $('.svncomm_content').html($('.svncomm_content').html()+msg.content.text.replace(/(?:\r\n|\r|\n)/g, '<br />'))
                            if (msg.content.text.indexOf('Committed revision')!==-1) {
                            $('.svncomm_content').removeClass('text-danger')
                            $('.svncomm_content').addClass('text-success')
                            $('.svn_success').text('Success');
                            }else {
                                $('.svncomm_content').removeClass('text-success')
                                $('.svncomm_content').addClass('text-danger')
                            }
                            
                        }
                        var kernel = Jupyter.notebook.kernel;
                        if (!commit_text) {commit_text=""}
                        notebook_path = project_dir+"/"+Jupyter.notebook.notebook_path.replaceAll(" ", "\\ ")
                        kernel.execute("!cd "+project_dir+" && svn add "+notebook_path+" --force && svn commit -m \""+commit_text+"\" "+notebook_path, cb);
                        
                                            
                        
                }
                })
                }
    
    
                dialog.modal({
                    body: div ,
                    title: 'Svn Commit',
                    buttons: {'Commit':
                                { class:'btn-primary btn-large',
                                  click:on_ok
                                },
                              'Cancel':{}
                        },
                    notebook:env.notebook,
                    keyboard_manager: env.notebook.keyboard_manager,
                })
            }
        }
        
        
    
        function _on_load(){
    
            // log to console
            console.info('Loaded Jupyter extension: Svn Up Commit')
    
            // register new action
            var action_name = IPython.keyboard_manager.actions.register(svn_up, 'up', 'jupyter-svn')
            
            
            var menu_item = $('<li/>').append(
                $('<a/>', {
                    'title' : 'Svn Up',
                    'id'   : 'Svn Up',
                    'href'     : '#',
                    'data-jupyter-action': 'jupyter-svn:up',
                    'onclick': 'IPython.keyboard_manager.actions.call("jupyter-svn:up")'
                })
                .append($('<span/>').html('Svn Up'))
            );
    
            var edit_menu = $('#file_menu');
            edit_menu.append($('<li/>').addClass('divider'));
            edit_menu.append(menu_item);
            
            var action_name = IPython.keyboard_manager.actions.register(svn_commit, 'commit', 'jupyter-svn')
            
                    var menu_item = $('<li/>').append(
                $('<a/>', {
                    'title' : 'Svn Commit',
                    'id'   : 'Svn Commit',
                    'href'     : '#',
                    'data-jupyter-action': 'jupyter-svn:commit',
                    'onclick': 'IPython.keyboard_manager.actions.call("jupyter-svn:commit")'
                })
                .append($('<span/>').html('Svn Commit'))
            );
            var edit_menu = $('#file_menu');
             edit_menu.append(menu_item);
    
        }
    
        return {load_ipython_extension: _on_load };
    })
    