define(['jquery', 'base/js/namespace'], function ($, Jupyter) {
    "use strict";
console.debug(Jupyter.notebook.notebook_path)
    var load_ipython_extension = function () {
        var menu_item = $('<li/>').append(
            $('<a/>', {
                'title' : 'publish notebook',
                'id'   : 'publish_notebook',
                'href'     : '#',
            })
            .append($('<span/>').html('Publish notebook'))
        );

        var edit_menu = $('#file_menu');
        edit_menu.append($('<li/>').addClass('divider'));
        edit_menu.append(menu_item);
 $('#publish_notebook').click(function() {
$('#save_checkpoint a').click();
var path_base = '/var/www/html/explore/research';
window.path_base = path_base
var url = window.location.href;
var pb_data = url.split("/");
var notebook_name = pb_data.slice(-1)[0]
notebook_name = notebook_name.split('#', 1)[0].split('?', 1)[0]
var ext_path = "/home/"+pb_data[4]+"/projects/"+Jupyter.notebook.notebook_path
window.ext_path = ext_path;
doModal('Enter a save path', '<div class="input-group"><span class="input-group-addon">'+path_base+'</span><input type="text" class="form-control pb_folder" value="/'+pb_data[4]+'/'+notebook_name.replace('ipynb','html').replace(/%20/g, '_')+'"></div>');
});
$('body').on('click','.pb-approve',function(ev) {
var servername = window.location.hostname.replace('jupyter-', '').replace('.appliedalpha.com', '');
var destination_path = window.path_base+$('.pb_folder').val()
$('#dynamicModal .btn').remove();
$('#dynamicModal .modal-body').html('<i class="fa fa-spinner fa-spin fa-3x center-block"></i>');
$.ajax({
type: "POST",
url: "//"+servername+".appliedalpha.com/explore/ajax.php",
data: {type:"publish_notebook", destination_path:destination_path, notebook_path:window.ext_path}
})
.always(function(msg) {
$('#dynamicModal .modal-body').html(msg)
});
});

function doModal(heading, formContent) {
    html =  '<div id="dynamicModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '<h4>'+heading+'</h4>'
    html += '</div>';
    html += '<div class="modal-body text-center">';
    html += formContent;
    html += '</div>';
    html += '<div class="modal-footer">';
    html += '<span class="btn btn-primary pull-left pb-approve">Ok</span>';
    html += '<span class="btn btn-primary pull-left" data-dismiss="modal">Cancel</span>';
    html += '</div>';  // content
    html += '</div>';  // dialog
    html += '</div>';  // footer
    html += '</div>';  // modalWindow
    $('body').append(html);
    $("#dynamicModal").modal();
    $("#dynamicModal").modal('show');

    $('#dynamicModal').on('hidden.bs.modal', function (e) {
        $(this).remove();
    });
    IPython.notebook.keyboard_manager.enabled = false

}


function hideModal()
{
    // Using a very general selector - this is because $('#modalDiv').hide
    // will remove the modal window but not the mask
    $('.modal.in').modal('hide');
}

    };

    // export the extension so it can be loaded correctly
    var extension = {
        load_ipython_extension : load_ipython_extension
    };
    return extension;
});






